# PySwapi

Python + Flask Sample Web Application: search films from Star Wars API.

[Demo: See it in action](http://monica-pyswapi.herokuapp.com/)