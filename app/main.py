# -*- coding: utf-8 -*-

import os
import sys
from config import *
from filmsModule import filmsModule
from flask import Blueprint
from service.SwapiService import *

app.register_blueprint(filmsModule, url_prefix='/films')

@app.route("/")
def default():
    return render_template("initial.html")

@app.route('/login', methods=['GET','POST'])
def login():
    return render_template('login.html')


@app.route('/index', methods=['GET', 'POST'])
def findAll():
    if 'logged_in' in session:
        return redirect(url_for('films.findAll'))
    else:
        return redirect(url_for('login'))


@app.route('/form', methods=['GET', 'POST'])
def form():
    if 'logged_in' in session:
        return redirect(url_for('films.findAll'))
    else:
        return redirect(url_for('login'))

@app.errorhandler(404)
@app.errorhandler(503)
@app.errorhandler(401)
@app.errorhandler(400)
@app.errorhandler(500)
@app.errorhandler(403)
@app.errorhandler(405)
def default_error_page(e):
    return render_template('error.html', error = e)

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)

