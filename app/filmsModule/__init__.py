# -*- coding: utf-8 -*-

import json
from flask import *
from config import *
from flask_restful import *
from flask_cors import CORS
from service.SwapiService import *

filmsModule = Blueprint('films', __name__, template_folder='templates', static_folder='assets')

@filmsModule.route("/index")
def index():
    return redirect(url_for('films.findAll'))


@filmsModule.route('/login', methods=['GET', 'POST'])
def login():
    # TODO: connect to login service
    # TODO: redirect to desired page after login
        session['logged_in'] = True
        session['user'] = request.form['nome']
        return redirect(url_for('films.findAll'))


@filmsModule.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('default'))


@filmsModule.route("/form")
def form():
    if 'logged_in' in session:
        return render_template("form.html" )
    else:
        return redirect(url_for('login'))


@filmsModule.route("/add", methods=['GET','POST'])
def add():
    if request.method == 'GET':
        return redirect(url_for('films.form'))
    if 'logged_in' in session:
        if request.form['release_date'] != '' and request.form['title'] != '':
                try:                     
                    # TODO: Persist films
                    # TODO: Add Characters, Vehicles, Planets, Starships and Species
                    # TODO: Improve fields validation
                    movie = {'title': request.form['title'], 
                            'episode_id': request.form['episode_id'],
                            'opening_crawl': request.form['opening_crawl'], 
                            'director': request.form['director'], 
                            'producer': request.form['producer'], 
                            'release_date': request.form['release_date']}
                    lista = SwapiService.addToList(movie, SwapiService.findAll())
                    return render_template('films.html', movies=lista)
                except Exception:
                    return render_template('error.html', error="503: Service Unavaliable. Try again later...")
        return redirect(url_for('films.findAll'))
    else:
        return redirect(url_for('login'))


@filmsModule.route("/find", methods=['GET','POST'])
def find():
    if request.method == 'GET' :
        return redirect(url_for('films.findAll'))
    if 'logged_in' in session:
        try:
            if request.form['title'] != '':
                lista = SwapiService.find(request.form['title'])
            else:
                lista = SwapiService.findAll()
            return render_template('films.html', movies=lista)
        except Exception:
            return render_template('error.html', error="503: Service Unavaliable. Try again later...")
    else:
        return redirect(url_for('login'))

# TODO: Add links to Characters, Vehicles, Planets, Starships and Species details

@filmsModule.route("/details", methods=['POST', 'GET'])
def details():
    if 'logged_in' in session:
        if request.args['url'] != '':
            try:
                movie = SwapiService.details(request.args['url'])
            except Exception:
                return render_template('error.html', error="503: Service Unavaliable. Try again later...")
        else:
            return redirect(url_for('films.findAll'))
        return render_template('film-details.html', movie=movie, imageUrl=app.config['IMAGES_BASE_URL'])
    else:
        return redirect(url_for('login'))


@filmsModule.route('/')
def findAll():
    if 'logged_in' in session:
        try:
            lista = SwapiService.findAll()
            return render_template('films.html', movies=lista)
        except Exception:
            return render_template('error.html', error="503: Service Unavaliable. Try again later...")
    else:
        return redirect(url_for('login'))


@filmsModule.errorhandler(404)
@filmsModule.errorhandler(503)
@filmsModule.errorhandler(401)
@filmsModule.errorhandler(400)
@filmsModule.errorhandler(500)
@filmsModule.errorhandler(403)
@filmsModule.errorhandler(405)
def default_error_page(e):
    return render_template('error.html', error=e)