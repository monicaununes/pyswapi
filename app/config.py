import os
from flask import *

app = Flask(__name__, static_folder='filmsModule/assets')

app.config['SWAPI_BASE_URL'] = "https://swapi.dev/api/"
app.config['IMAGES_BASE_URL'] = "https://sulky-witness.glitch.me/sw/"

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,çRT'
