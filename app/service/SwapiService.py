# -*- coding: utf-8 -*-
import sys
import requests
import collections
from config import *
from flask_cors import CORS
from flask import Flask, json
from flask_restful import Resource, Api

class SwapiService:
        
    @staticmethod
    def findAll():
        url = app.config['SWAPI_BASE_URL'] + "films/"
        request = requests.get(url)
        movies = json.loads(request.text)['results']
        movies = sorted(movies, key = lambda date: date["release_date"], reverse = True)
        return (movies)

    @staticmethod
    def details(episode_id):
       # url = app.config['SWAPI_BASE_URL'] + "films/" + episode_id
        url = episode_id
        request = requests.get(url)
        movies = json.loads(request.text)
        return movies
    
    @staticmethod
    def find(title):
        url = app.config['SWAPI_BASE_URL'] + "films/?search=" + title
        request = requests.get(url)
        movies = json.loads(request.text)['results']
        movies = sorted(movies, key = lambda date: date["release_date"], reverse = True)
        return movies
        
    @staticmethod
    def addToList(movie, movies):
        movies.append(movie)
        movies = sorted(movies, key = lambda date: date["release_date"], reverse = True)
        return (movies)
    
